﻿/*
Keyboard shortcuts of pgAdmin 1.20.0 on Windows 7 32bit

usage example: 
1. On any database execute the following SQL as it it.
2. Insert "CREATE TABLE or VIEW ... AS " before "SELECT" below and execute it,
   and you have a table or view of keyboard shortcuts.

by kenpg, 2015, CC0 license
*/

SELECT 
    CASE WHEN column1 <= 5 THEN 'Object Browser'
        ELSE 'SQL Editor' END :: text AS place,

    CASE column1 WHEN 2 THEN 'an object'
        WHEN 3 THEN 'a table'
        WHEN 4 THEN 'a row of propaties pane'
        WHEN 5 THEN 'SQL pane'
        WHEN 7 THEN 'editor'
        ELSE '' END :: text AS "select or fucus",

    column2[1] AS "key shortcut",
    column2[2] AS action,
    column2[3] = '1' AS "exists in menu",
    column2[4] AS notes

FROM (VALUES

-- on object browser
(1, '{Ctrl-Q,     "Exit",           1, ""}' :: text[]),
(1, '{Ctrl-G,     "Search objects", 1, ""}'),
(1, '{Ctrl-Alt-O, "object browser", 1, ""}'),
(1, '{Ctrl-Alt-S, "SQL pane",       1, ""}'),
(1, '{Ctrl-Alt-T, "Tool bar",       1, ""}'),
(1, '{Ctrl-Alt-V, "Default view",   1, ""}'),
(1, '{F5,         "Refresh",        1, ""}'),
(1, '{Ctrl-E,     "Query tool",     1, ""}'),

-- on object browser with an object selected
(2, '{Del,            "Delete",     1, ""}'),
(2, '{Ctrl-Alt-Enter, "Properties", 1, ""}'),

-- on object browser with a table selected
(3, '{Ctrl-D, "View all rows", 1, ""}'),

-- on object browser with a row of propaties pane selected
(4, '{Ctrl-C, "Copy a propertiy and a value", 1,
    "two columns are TAB separated"}'),

-- on object browser with an object selected and SQL pane focused
(5, '{Ctrl-C, "Copy selected characters", 1, ""}'),
(5, '{Ctrl-A, "select all",               0, ""}'),
(5, '{Ctrl-F, "open a search dialog",     0, ""}'),
(5, '{Ctrl-K, "same as a down arrow key", 0, ""}'),
(5, '{Ctrl-L, "select a row",             0, ""}'),
(5, '{Ctrl-V,
    "move right or down by lengths of characters in clipboard", 0,
    "just a result of pasting to a read-only pane"}'),
(5, '{Ctrl-/, "move one word left",       0, ""}'),

-- on SQL Editor of Query tool
(6, '{Ctrl-N,       "New window",       1, ""}'),
(6, '{Ctrl-O,       "Open",             1, ""}'),
(6, '{Ctrl-S,       "Save",             1, ""}'),
(6, '{Ctrl-W,       "Exit",             1, ""}'),
(6, '{Ctrl-Z,       "Undo",             1, ""}'),
(6, '{Ctrl-Y,       "Redo",             1, ""}'),
(6, '{Ctrl-X,       "Cut",              1, ""}'),
(6, '{Ctrl-C,       "Copy",             1, ""}'),
(6, '{Ctrl-V,       "Paste",            1, ""}'),
(6, '{Ctrl-F,       "Find and Replace", 1, ""}'),
(6, '{Ctrl-U,       "Upper case",       1, ""}'),
(6, '{Ctrl-Shift-U, "Lower case",       1, ""}'),
(6, '{Tab,          "Block Indent",     1, ""}'),
(6, '{Shit-Tab,     "Block Outdent",    1, ""}'),
(6, '{Ctrl-K,       "Comment Text",     1, ""}'),
(6, '{Ctrl-Shift-K, "Uncomment Text",   1, ""}'),
(6, '{F5,           "Execute",          1, ""}'),
(6, '{F6,           "Execute pgScript", 1, ""}'),
(6, '{F8,           "Execute to file",  1, ""}'),
(6, '{F7,           "Explain",          1, ""}'),
(6, '{Shift-F7,     "Explain analyze",  1, ""}'),
(6, '{Alt-Break,    "Cancel",           1, ""}'),
(6, '{Ctrl-Alt-B,   "Connection bar",   1, ""}'),
(6, '{Ctrl-Alt-O,   "Output pane",      1, ""}'),
(6, '{Ctrl-Alt-S,   "Scratch pad",      1, ""}'),
(6, '{Ctrl-Alt-T,   "Tool bar",         1, ""}'),
(6, '{Ctrl-Alt-V,   "Default view",     1, ""}'),
(6, '{F1,           "SQL Help",         1, ""}'),
(6, '{F2,           "Inject to favorites", 1,
    "Details unknown to me"}'),
(6, '{Ctrl-E,       "Execute",          0, ""}'),
(6, '{Ctrl-A,       "select all",       0, ""}'),
(6, '{Ctrl-Space,   "input completion", 0, ""}'),

-- on SQL Editor with edit pane focused
(7, '{Ctrl-D,   "copy a row",                        0, ""}'),
(7, '{Ctrl-L,   "delete a row",                      0, ""}'),
(7, '{Ctrl-T,   "replace a row with previous one",   0, ""}'),
(7, '{Ctrl-/,   "move one word left",                0, ""}'),
(7, '{Ctrl-Tab, "change to Graphical Query Builder", 0, ""}')

) foo;
