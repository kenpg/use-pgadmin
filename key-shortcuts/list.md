The result of list.sql are below:  
![ex1](//bitbucket.org/kenpg/use-pgadmin/raw/master/key-shortcuts/images/list-ex1.png)  

usage example: SELECT ... WHERE "key shortcut" LIKE '%L'  
![ex2](//bitbucket.org/kenpg/use-pgadmin/raw/master/key-shortcuts/images/list-ex2.png)  

usage example: SELECT ... WHERE "exists in menu" IS FALSE  
![ex3](//bitbucket.org/kenpg/use-pgadmin/raw/master/key-shortcuts/images/list-ex3.png)
