﻿;
; Select one SQL statement including the caret on pgAdmin Query tool
;
; This is replaced by select_current_sql_v2.ahk and almost useless.
;
; Tested on
;     Windows 7 32bit,
;     AutHotkey Unicode 32bit 1.1.20.3,
;     pgAdmin Portable 1.20.0
; 
; By kenpg, 2015, http://bitbucket.org/kenpg/use-pgadmin/
; CC0 license
;

#SingleInstance force
#UseHook

^1::Reload

SetTitleMatchMode, 1 ; left-hand match
#IfWinActive, Query

    ; Ctrl＋Shift＋A
    ^+A::
    {
        bak := ClipboardAll
        Send,{End}+{Home 2}
        cy_pre := % A_CaretY
        Loop
        {
            Send,{Up}
            cy := % A_CaretY
            if % cy == cy_pre ; A caret came to the top of pane
                break

            Send,{End}+{Home 2}
            if CheckRow() == 1
            {
                Send,{Down}
                break
            }
            cy_pre := cy
        }
        Loop
        {
            Send,+{End}
            if CheckRow() == 1
                break

            Send,+{Right}
            cy := % A_CaretY
            if % cy == cy_pre ; A caret came to the bottom of pane
                break
            cy_pre := cy
        }
        Clipboard = % bak
        Return
    }

CheckRow() {
    Clipboard = 
    Send,^c
    ; ClipWait,1 ; omit to speed up
                 ; reconsider if there are any problems
    str := Clipboard
    if StrLen(str) == 0 || InStr(str, ";") > 0
        Return 1
    if RegExMatch(str, "^\s+$", $)
        Return 1
    Return 0
}
