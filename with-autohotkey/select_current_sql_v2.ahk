﻿;
; Select one SQL statement including the caret on pgAdmin Query tool
;
; This is a revision of pgadmin_select_current_sql.ahk
;
; Tested on
;     Windows 7 32bit,
;     AutHotkey Unicode 32bit 1.1.20.3,
;     pgAdmin Portable 1.20.0
; 
; By kenpg, 2015, http://bitbucket.org/kenpg/use-pgadmin/
; CC0 license
;

#SingleInstance force
#UseHook

^2::Reload

SetTitleMatchMode, 1 ; left-hand match
#IfWinActive, Query

    ; Ctrl＋Shift＋Q
    ^+Q::
    {
        bak := ClipboardAll        
        Clipboard =

        Send,{Home 2}
        Send,^+{Home}
        Send,^c
        ClipWait,1
        str1 := Clipboard
        
        Send,^+{End}
        Send,^c
        ClipWait,1
        str2 := Clipboard

        Clipboard = % bak
        
        str1 := RegExReplace(str1, "^[\s\S]*;\s*(--.*)*\n*")
        up := StrLen(RegExReplace(str1, "[^\n]", , , -1))

        str2 := RegExReplace(str2, ";\s*(--.*)*\n*[\s\S]*$", ";")
        str2 := RegExReplace(str2, "\n+$")
        down := StrLen(RegExReplace(str1 . str2, "[^\n]", , , -1)) + 1

        Send,{Left}{Home 2}{Up %up%}+{Down %down%}
        If % str2 == ""
            Send,^+{End}
    }
