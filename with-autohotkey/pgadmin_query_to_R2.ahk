﻿;
; pgAdminクエリツール上で書いたRスクリプトを
; Rコンソールへ貼り付け＆実行するテスト 2
;
; Encoding: UTF-8 with BOM
;
; Tested on
;     Windows 7 32bit,
;     AutHotkey Unicode 32bit 1.1.20.3,
;     pgAdmin Portable 1.20.0 and
;     R Portable 3.1.2
; 
; By kenpg, 2015, http://bitbucket.org/kenpg/use-pgadmin/
; CC0 license
;

#SingleInstance force
Menu, TRAY, Icon, % A_AhkPath, 5
^1::Reload

; ウィンドウタイトルを前方一致検索
SetTitleMatchMode, 1

; ウィンドウタイトルで pgAdmin クエリツール上に限定
; 必要に応じて Query - dbname on username など詳しく指定可
#IfWinActive, Query

    ; Ctrl＋R …選択文字列をRコンソールに貼り付け、最後にENTER
    ^R::
    {
        PasteToR()
        Return
    }

    ; Ctrl＋Shift＋R …上と同じで、最後にRコンソールを前面化
    ^+R::
    {
        PasteToR()
        WinActivate, R Console
        Return
    }

    ; Ctrl＋L …Rコンソールをクリア
    ; pgAdmin デフォルトの Ctrl＋L は無効になる
    ^L::
    {
        IfWinExist, R Console
        {
            ControlSend,,^l,R Console
            Return
        }
    }

    ; Ctrl＋Shift＋L …Rコンソールを終了
    ^+L::
    {
        IfWinExist, R Console
        {
            ControlSend,,q(){ENTER},R Console
            Return
        }
    }

PasteToR()
{
    ; Rプログラムまたは起動用バッチファイル等を指定
    rpath  = "D:\AppsPortable\R-Portable\R-Portable.cmd"

    ; 実行直前の内容を控え、クリップボードを空にする
    bak = % ClipboardAll
    Clipboard =

    Send,^c
    If (Clipboard = "")
    {
        Return
    }
    IfWinNotExist, R Console
    {
        Run, % rpath
        WinWait, R Console
    }
    ControlSend,,^v{ENTER},R Console

    ; 実行直前のクリップボードに戻す
    Clipboard = % bak
}
