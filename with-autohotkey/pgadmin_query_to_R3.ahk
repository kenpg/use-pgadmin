﻿;
; pgAdminクエリツール上で書いたRスクリプトを
; Rコンソールへ貼り付け＆実行するテスト 3
; 文字列を選択していない時、/* R ～ */ のブロックがあればその中を渡す
;
; Encoding: UTF-8 with BOM
;
; Tested on
;     Windows 7 32bit,
;     AutHotkey Unicode 32bit 1.1.20.3,
;     pgAdmin Portable 1.20.0 and
;     R Portable 3.1.2
; 
; By kenpg, 2015, http://bitbucket.org/kenpg/use-pgadmin/
; CC0 license
;

#SingleInstance force
#UseHook
Menu, TRAY, Icon, % A_AhkPath, 5
^1::Reload

; ウィンドウタイトルを前方一致検索
SetTitleMatchMode, 1

; ウィンドウタイトルで pgAdmin クエリツール上に限定
; 必要に応じて Query - dbname on username など詳しく指定可
#IfWinActive, Query

    ; Ctrl＋R …選択文字列をRコンソールに貼り付け、最後にENTER
    ^R::
    {
        PasteToR()
        Return
    }

    ; Ctrl＋Shift＋R …上と同じで、最後にRコンソールを前面化
    ^+R::
    {
        PasteToR()
        WinActivate, R Console
        Return
    }

    ; Ctrl＋L …Rコンソールをクリア
    ; pgAdmin デフォルトの Ctrl＋L は無効になる
    ^L::
    {
        IfWinExist, R Console
        {
            ControlSend,,^l, R Console
            Return
        }
    }

    ; Ctrl＋Shift＋L …Rコンソールを終了
    ^+L::
    {
        IfWinExist, R Console
        {
            ControlSend,,q(){ENTER}, R Console
            Return
        }
    }

PasteToR()
{
    ; Rプログラムまたは起動用バッチファイル等を指定
    rpath := "D:\AppsPortable\R-Portable\R-Portable.cmd"

    ; 実行直前の内容を控え、クリップボードを空にする
    bak := % ClipboardAll
    Clipboard =

    Send,^c
    ; ClipWait,1 ; 入れてみたが、Ctrl-V失敗は解消せず
    If (Clipboard = "")
    {
        src := GetRCode()
        If (StrLen(src) == 0)
            Return
        Clipboard = % src
    }
    IfWinNotExist, R Console
    {
        Run, % rpath
        WinWait, R Console
    }
    ControlSend,,^v{ENTER}, R Console

    ; 実行直前のクリップボードに戻す
    Clipboard = % bak
}

GetRCode()
{
    cx := % A_CaretX
    cy := % A_CaretY

    Send,^a^c
    ; ClipWait,1 ; 遅くなるので省き、問題あれば再検討
    Send,{Left}  ; 文字列選択を解除
    RegExMatch(Clipboard, "/\* R([\s\S]+?)\*/", $)
    Loop
    {
        if A_CaretY >= % cy
            break
        Send,{Down}
    }
    Loop
    {
        if A_CaretX >= % cx
            break
        Send,{Right}
    }
    Return, % $1
}
